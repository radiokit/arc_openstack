defmodule ArcOpenstack do

  use Application

  def start(_type, _args) do
    HTTPoison.start

    [endpoint_url: keystone_endpoint_url,
     tenant_id: keystone_tenant_id,
     username: keystone_username,
     password: keystone_password ] =
   Application.get_env(:arc_openstack, :keystone)

    [endpoint_url: swift_endpoint_url,
     signing_key: swift_signing_key ] =
   Application.get_env(:arc_openstack, :swift)

   OpenStax.Keystone.Endpoint.register_password(
     :ovh, :"2.0",
     keystone_endpoint_url, keystone_tenant_id,
     nil,
     keystone_username, keystone_password)

   OpenStax.Swift.Endpoint.register(
     :ovh,
     (fn -> OpenStax.Keystone.Endpoint.get_auth_token(:ovh) end),
     swift_endpoint_url, swift_signing_key)

     keystone_pid = Process.whereis(OpenStax.Keystone)
     {:ok, keystone_pid}
  end
end
