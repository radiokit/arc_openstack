defmodule ArcOpenstack.Definition do
  @callback storage_object_id(any, {any, any}) :: String.t
  @callback chunk_object_prefix(any, {any, any}) :: String.t

  defmacro __using__(_options) do
    quote do
      def __storage, do: ArcOpenstack.Storage

      def before_chunks(definition, version, {file, scope}), do: {:ok, nil}
      def before_chunk(definition, version, {file, scope}, chunk, index), do: {:ok, nil}
      def after_chunk(definition, version, {file, scope}, chunk, index, response), do: {:ok, nil}
      def after_chunks(definition, version, {file, scope}), do: {:ok, nil}

      def after_manifest(definition, scope, response), do: {:ok, nil}

      def before_put(definition, version, {file, scope}), do: {:ok, nil}
      def after_put(definition, version, {file, scope}, response), do: {:ok, nil}

      def chunk_object_prefix(version, {scope, file}), do: ""

      def chunk_object_id(version, {scope, file}, number) do
        number_as_string = to_string(number)
        number_with_padding = String.duplicate("0", 32 - byte_size(number_as_string)) <> number_as_string

        "#{chunk_object_prefix(version, {scope, file})}_#{number_with_padding}"
      end

      defoverridable [
        before_chunks:       3,
        before_chunk:        5,
        after_chunk:         6,
        after_chunks:        3,
        after_manifest:      3,
        before_put:          3,
        after_put:           4,
        chunk_object_id:     3,
        chunk_object_prefix: 2,
      ]
    end
  end
end
