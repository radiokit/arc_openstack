defmodule ArcOpenstack.Storage do

  @storage_backend_id :ovh # FIXME move to config
  @storage_container  :vault # FIXME move to config
  @signing_key_expires 3600
  @size_to_chunk 10_000_000
  @chunk_size 10_000_000

  def put(definition, version, {file, nil}) do
    file_name = file.file_name
    put(definition, version, {file, %{id: file_name}})
  end
  def put(definition, version, {file, scope}) do
    {:ok, %{size: file_size}} = File.stat(file.path)

    if file_size >= @size_to_chunk do
      put_chunked(definition, version, {file, scope})
    else
      put_object(definition, version, {file, scope})
    end
  end

  def put_chunked(definition, version, {file, scope}) do
    case definition.before_chunks(definition, version, {file, scope}) do
      {:error, reason} -> {:error, reason}
      {:ok, _} ->
        File.stream!(file.path, [], @chunk_size)
        |> Stream.with_index
        |> Enum.map(fn
          ({chunk, index}) ->
            object_id = definition.chunk_object_id(version, {file, scope}, index)
            case definition.before_chunk(definition, version, {file, scope}, chunk, index) do
              {:error, reason} -> {:error, reason}
              {:ok, _} ->
                response = do_put_object(definition, object_id, chunk, headers: content_type_headers(file))
                definition.after_chunk(definition, version, {file, scope}, chunk, index, response)
            end
        end)
      put_dlo_manifest(definition, version, {file, scope})
    end
  end

  def put_dlo_manifest(definition, version, {file, scope}) do
    OpenStax.Swift.API.Object.create_dlo_manifest(
      @storage_backend_id, @storage_container,
      definition.storage_object_id(version, {file, scope}),
      @storage_container,
      definition.chunk_object_prefix(version, {file, scope}),
       "application/octet-stream",
       "attachment",
       file.file_name
     )
     |> case do
          {:error, reason} -> {:error, reason}
       {:ok, details} ->
         case definition.after_manifest(definition, scope, details) do
           {:ok, _} -> {:ok, definition.storage_object_id(version, {file, scope})}
           {:error, reason} -> {:error, reason}
         end
     end
  end

  def put_object(definition, version, {file, scope}) do
    with object_id <- definition.storage_object_id(version, {file, scope}),
    {:ok, contents} <- File.read(file.path)
    do
      case definition.before_put(definition, version, {file, scope}) do
        {:error, reason} -> {:error, reason}
        {:ok, _} ->
          response = do_put_object(definition, object_id, contents, headers: content_type_headers(file))
          case definition.after_put(definition, version, {file, scope}, response) do
            {:error, reason} -> {:error, reason}
            {:ok, nil} -> {:ok, object_id}
            {:ok, other} -> {:ok, other}
          end
      end
    end
  end

  def do_put_object(definition, object_id, contents, options \\ []) do
    case OpenStax.Swift.Request.request(@storage_backend_id, :put, [to_string(@storage_container), to_string(object_id)], [201], [ body: contents ] ++ options) do
      {:ok, code, headers, body} ->
        {"Etag", etag} = List.keyfind(headers, "Etag", 0)

        {:ok, %{ etag: etag }}

      {:error, reason} ->
        {:error, reason}
    end
    |> case do
          {:ok, %{etag: etag}} -> {:ok, object_id}
          {:error, reason} -> {:error, reason}
    end
  end

  defp content_type_headers(file) do
    [{"X-Detect-Content-Type", true}, {"Content-Disposition", "attachment; filename=\"#{file.file_name}\""}]
    []
  end

  def url(definition, version, file_scope, options \\ [])

  def url(definition, version, {file, nil}, options) do
    file_name = file.file_name
    url(definition, version, {file, %{id: file_name}}, options)
  end
  def url(definition, version, {file, scope}, options) do
    OpenStax.Swift.Middleware.TempURL.generate(
      @storage_backend_id, @storage_container,
      definition.storage_object_id(version, {file, scope}), @signing_key_expires)
  end

  def delete(definition, version, {file, nil}) do
    file_name = file.file_name
    delete(definition, version, {file, %{id: file_name}})
  end
  def delete(definition, version, {file, scope}) do
    OpenStax.Swift.API.Object.delete(
      @storage_backend_id, @storage_container,
      definition.storage_object_id(version, {file, scope}))
  end
end
