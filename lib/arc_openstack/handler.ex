defmodule ArcOpenstack.Handler do

  @arc_url_regex ~r{\Ahttps?://}
  @raw_file_regex ~r(\Adata:image/jpeg;base64,)
  @raw_file_prefix "data:image/jpeg;base64,"

  def file_value?(value) when is_bitstring(value) do
    value =~ @arc_url_regex || value =~ @raw_file_regex
  end

  def file_value?(_field), do: false

  def download(field, value) do
    file_path =
      cond do
        value =~ @arc_url_regex ->
          ArcOpenstack.Downloader.get(value)
        value =~ @raw_file_regex ->
          << @raw_file_prefix , content :: bitstring >> = value
          {:ok, path} = Tempfile.random("file.jpg")
          {:ok, decoded_content} = Base.decode64(content)

          case File.write(path, decoded_content) do
            :ok -> path
            {:error, reason} -> nil
          end
          true -> nil
      end
  end
end
