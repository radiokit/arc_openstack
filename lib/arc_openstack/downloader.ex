defmodule ArcOpenstack.Downloader do
  @recv_timeout 1000*60*10
  require Logger

  def get(url) do
    {:ok, temp_file_name} = Tempfile.random("arc_download")
    {:ok, file_pid} = File.open(temp_file_name, [:append])

    case HTTPoison.get(url, [], [stream_to: self, recv_timeout: @recv_timeout]) do
      {:ok, %{id: ref}} ->
        file_receive_loop({temp_file_name, file_pid, url})
        other -> nil
    end
  end

  def file_receive_loop({name, file, file_url}) do
    receive do
      %HTTPoison.AsyncStatus{code: 200, id: id} ->
        Logger.info("Downloading #{file_url} #{inspect(id)}")
        file_receive_loop({name, file, file_url})
      %HTTPoison.AsyncStatus{code: 401} ->
        Logger.warn "Unauthorized! #{file_url}"
        :error
      %HTTPoison.AsyncStatus{code: 404} ->
        Logger.warn "File doesn't exist! #{file_url}"
        :error
      %HTTPoison.AsyncChunk{chunk: chunk, id: id} ->
        Logger.debug("Chunk #{inspect(id)}")
        IO.binwrite(file, chunk)
        file_receive_loop({name, file, file_url})
      %HTTPoison.AsyncEnd{id: id} ->
        Logger.info("Downloaded #{file_url}, #{inspect(id)}}")
        File.close(file)
        name
      %HTTPoison.AsyncHeaders{id: id} ->
        file_receive_loop({name, file, file_url})
      other ->
        Logger.info("Unexpected during #{file_url}: #{inspect(other)}}")
       :error
    end
  end
end
