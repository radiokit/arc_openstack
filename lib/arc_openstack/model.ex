defmodule ArcOpenstack.Model do

  defmacro __using__(options) do
    required_file_fields = Keyword.get(options, :required, [])
    optional_file_fields = Keyword.get(options, :optional, [])

    quote(location: :keep) do
      @arc_files_fields unquote(required_file_fields ++ optional_file_fields)

      before_insert :_handle_file_fields
      before_update :_handle_file_fields

      after_insert :_upload_on_insert
      before_update :_upload_on_update

      def cast_arc_openstack(changeset) do
        Enum.reduce(@arc_files_fields, changeset, fn(field, changeset) -> 
         if Map.has_key?(changeset.params, field) do
           put_change(changeset, field, changeset.params[field])
         else
           changeset
         end
        end)
      end

      def _handle_file_fields(%{params: nil} = changeset), do: changeset
      def _handle_file_fields(changeset) do
        new_params =
          changeset.params
          |> Enum.map(fn
            {field, value} when field in @arc_files_fields and is_bitstring(value) ->
              cond do
                ArcOpenstack.Handler.file_value?(value) ->
                  case ArcOpenstack.Handler.download(field, value) do
                    nil -> 
                       changeset = add_error(changeset, field, :error)
                       {field, nil}
                   file_path ->
                      {field, file_path}
                  end
                true ->
                  {field, value}
              end
              other -> other
          end)
        |> Enum.into(%{})

        put_in changeset.params, new_params
      end


      def _upload_on_insert(%{params: nil} = changeset), do: changeset
      def _upload_on_insert(changeset) do
        new_changeset = 
          cast_attachments(changeset.model, changeset.params, unquote(required_file_fields), unquote(optional_file_fields))

        new_changeset = Ecto.Changeset.change(new_changeset.model, new_changeset.changes)

        case changeset.repo.update(new_changeset) do
          {:ok, record} -> new_changeset
          other -> add_error(new_changeset, :params, other)
        end
      end

      def _upload_on_update(%{params: nil} = changeset), do: changeset
      def _upload_on_update(changeset) do
        cast_attachments(changeset, changeset.params, unquote(required_file_fields), unquote(optional_file_fields))
      end
    end
  end
end
