# ArcOpenstack
Implements OpenStack storage for Arc uploader.

Usage:

```elixir
config :arc_openstack, :keystone,
  endpoint_url: KEYSTONE_ENDPOINT_URL,
  tenant_id:    KEYSTONE_TENANT_ID,
  username:     KEYSTONE_USERNAME,
  password:     KEYSTONE_PASSWORD,

config :arc_openstack, :swift,
  endpoint_url:    SWIFT_ENDPOINT_URL,
  signing_key:     SWIFT_SIGNING_KEY

# Attachment
defmodule MyApp.File do
  use Arc.Definition
  use Arc.Ecto.Definition
  use Vault.OpenstackStore.Definition

  @versions [:original]

  # File name in Openstack
  def storage_object_id(version, {file, scope}) do
    "Data.Record.File_#\{scope.id}"
  end

  def chunk_object_prefix(version, {file, scope}) do
    "Data.Record.File_#\{scope.id}_"
  end
end

# With Ecto and ArcEcto
defmodule MyApp.User do
  use Ecto.Model
  use Arc.Ecto.Model
  use Vault.OpenstackStore.Model, optional: ["file"], required: []

  schema "user" do
    field :file, MyApp.File.Type
  end
end
```

## Installation

If [available in Hex](https://hex.pm/docs/publish), the package can be installed as:

  1. Add arc_openstack to your list of dependencies in `mix.exs`:

        def deps do
          [{:arc_openstack, "~> 0.0.1"}]
        end

  2. Ensure arc_openstack is started before your application:

        def application do
          [applications: [:arc_openstack]]
        end

